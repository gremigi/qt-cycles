#include "MainWindow.h"

#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>
#include <QStyleFactory>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    app.setStyle(QStyleFactory::create("Fusion"));

    MainWindow window;
    window.setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            window.size(),
            qApp->desktop()->availableGeometry()
        )
    );
    window.show();

    return app.exec();
}

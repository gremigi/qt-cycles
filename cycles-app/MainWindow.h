#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAction>
#include <QMainWindow>
#include <QSlider>
#include "CyclesWidget.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() = default;

    void setScene(const QString &imageFile);

private slots:
    void onLoadImageAction();

private:

    CyclesWidget *cyclesWidget;

    QAction *loadImageAction;

    void createActions();
    void createMenus();
    void createComponents();
};

#endif // MAINWINDOW_H

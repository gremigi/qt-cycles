#include "MainWindow.h"

#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setWindowTitle("Demo");

    createActions();
    createMenus();
    createComponents();
}

void MainWindow::setScene(const QString &imageFile)
{
    cyclesWidget->setScene(imageFile);
}

void MainWindow::createActions()
{
    loadImageAction = new QAction{tr("&Load image"), this};
    connect(loadImageAction, &QAction::triggered, this, &MainWindow::onLoadImageAction);
}

void MainWindow::createMenus()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(loadImageAction);
}

void MainWindow::createComponents()
{
    cyclesWidget = new CyclesWidget{this};

    QVBoxLayout *layout = new QVBoxLayout{};
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(cyclesWidget);

    QWidget *containerWidget = new QWidget{this};
    containerWidget->setLayout(layout);

    setCentralWidget(containerWidget);
}

void MainWindow::onLoadImageAction()
{
    QString defaultFilter = tr("Images (*.jpg *.jpeg *.png *.tif)");
    QString imageFile = QFileDialog::getOpenFileName(this,
        tr("Add images"),
        nullptr,
        tr("Images (*.jpg *.jpeg *.png *.tif);; All files (*.*)"),
        &defaultFilter
    );
    if (!imageFile.isEmpty()) {
        cyclesWidget->setScene(imageFile);
    }
}

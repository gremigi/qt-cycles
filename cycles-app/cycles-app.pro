include(../common.pri)

QT += core widgets gui

TEMPLATE = app
TARGET = Cycles

HEADERS += \
    MainWindow.h

SOURCES += \
    Main.cpp \
    MainWindow.cpp

LIBS += $$LIB_CYCLES

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

# Qt Cycles integration #

This is a sample project showing how to compile and integrate Blender Cycles into the Qt framework.

Scene, object and camera are created programmatically.

It is possible to zoom into the scene using the mouse wheel and move the object using the left mouse button and drag.

![Qt Cycles Demo.png](https://bitbucket.org/repo/ad6Mjx/images/2373393826-Qt%20Cycles%20Demo.png)

Please note you need to tweak a bit the content of the file "common.pri" to match you platform configuration.
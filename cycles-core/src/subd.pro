include(../cycles-core-common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = subd

HEADERS += \
    subd/subd_dice.h \
    subd/subd_patch.h \
    subd/subd_patch_table.h \
    subd/subd_split.h

SOURCES += \
    subd/subd_dice.cpp \
    subd/subd_patch.cpp \
    subd/subd_patch_table.cpp \
    subd/subd_split.cpp

INCLUDEPATH +=
#    ../graph \
#    ../kernel \
#    ../kernel/svm \
#    ../render \
#    ../util

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target





include(../cycles-core-common.pri)
TEMPLATE = lib
CONFIG += staticlib
TARGET = kernel

HEADERS += \
    $$files(kernel/bvh/*.h) \
    $$files(kernel/closure/*.h) \
    $$files(kernel/filter/*.h) \
    $$files(kernel/geom/*.h) \
    $$files(kernel/kernels/cpu/*.h) \
    $$files(kernel/osl/*.h) \
    $$files(kernel/shaders/*.h) \
    $$files(kernel/split/*.h) \
    $$files(kernel/svm/*.h) \
    $$files(kernel/*.h)

SOURCES += \
    $$files(kernel/kernels/cpu/*.cpp)

INCLUDEPATH +=

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target


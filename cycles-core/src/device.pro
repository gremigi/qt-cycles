include(../cycles-core-common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = device

HEADERS += \
    device/device.h \
    device/device_denoising.h \
    device/device_intern.h \
    device/device_memory.h \
    #device/device_network.h \
    device/device_split_kernel.h \
    device/device_task.h

SOURCES += \
    device/device.cpp \
    device/device_cpu.cpp \
    #device/device_cuda.cpp \
    device/device_denoising.cpp \
    #device/device_multi.cpp \
    #device/device_network.cpp \
    #device/device_opencl.cpp \
    device/device_split_kernel.cpp \
    device/device_task.cpp

INCLUDEPATH += ../third_party/atomic \
    ../third_party/cuew/include

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

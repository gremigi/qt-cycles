include(../cycles-core-common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = graph

HEADERS += \
    graph/node.h \
    graph/node_enum.h \
    graph/node_type.h \
    #graph/node_xml.h

SOURCES += \
    graph/node.cpp \
    graph/node_type.cpp \
    #graph/node_xml.cpp

INCLUDEPATH += #../util

LIBS += $$LIB_OPENIMAGEIO

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target





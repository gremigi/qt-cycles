include(../cycles-core-common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = render

HEADERS += $$files(render/*.h)

SOURCES += $$files(render/*.cpp)

INCLUDEPATH += ../third_party/atomic
#    ../device \
#    ../graph \
#    ../kernel \
#    ../kernel/svm \
#    ../kernel/osl \
#    ../bvh \
#    ../subd \
#    ../util

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

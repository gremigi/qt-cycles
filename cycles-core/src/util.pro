include(../cycles-core-common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = util

HEADERS += \
    util/util_algorithm.h \
    util/util_aligned_malloc.h \
    #util/util_args.h \
    util/util_atomic.h \
    util/util_avxf.h \
    util/util_boundbox.h \
    util/util_color.h \
    util/util_debug.h \
    util/util_foreach.h \
    util/util_function.h \
    util/util_guarded_allocator.h \
    util/util_half.h \
    util/util_hash.h \
    util/util_image.h \
    util/util_image_impl.h \
    util/util_list.h \
    util/util_logging.h \
    util/util_map.h \
    util/util_math.h \
    util/util_math_cdf.h \
    util/util_math_fast.h \
    util/util_math_float2.h \
    util/util_math_float3.h \
    util/util_math_float4.h \
    util/util_math_int2.h \
    util/util_math_int3.h \
    util/util_math_int4.h \
    util/util_math_intersect.h \
    util/util_math_matrix.h \
    util/util_md5.h \
    util/util_opengl.h \
    util/util_optimization.h \
    util/util_param.h \
    util/util_path.h \
    util/util_progress.h \
    util/util_queue.h \
    util/util_set.h \
    util/util_simd.h \
    util/util_sky_model.h \
    util/util_sky_model_data.h \
    util/util_sseb.h \
    util/util_ssef.h \
    util/util_ssei.h \
    util/util_stack_allocator.h \
    util/util_static_assert.h \
    util/util_stats.h \
    util/util_string.h \
    util/util_system.h \
    util/util_task.h \
    util/util_texture.h \
    util/util_thread.h \
    util/util_time.h \
    util/util_transform.h \
    util/util_types.h \
    util/util_types_float2.h \
    util/util_types_float2_impl.h \
    util/util_types_float3.h \
    util/util_types_float3_impl.h \
    util/util_types_float4.h \
    util/util_types_float4_impl.h \
    util/util_types_int2.h \
    util/util_types_int2_impl.h \
    util/util_types_int3.h \
    util/util_types_int3_impl.h \
    util/util_types_int4.h \
    util/util_types_int4_impl.h \
    util/util_types_uchar2.h \
    util/util_types_uchar2_impl.h \
    util/util_types_uchar3.h \
    util/util_types_uchar3_impl.h \
    util/util_types_uchar4.h \
    util/util_types_uchar4_impl.h \
    util/util_types_uint2.h \
    util/util_types_uint2_impl.h \
    util/util_types_uint3.h \
    util/util_types_uint3_impl.h \
    util/util_types_uint4.h \
    util/util_types_uint4_impl.h \
    util/util_types_vector3.h \
    util/util_types_vector3_impl.h \
    util/util_vector.h \
    util/util_version.h \
    #util/util_view.h \
    util/util_windows.h \
    #util/util_xml.h

SOURCES += \
    util/util_aligned_malloc.cpp \
    util/util_debug.cpp \
    util/util_guarded_allocator.cpp \
    util/util_logging.cpp \
    util/util_math_cdf.cpp \
    util/util_md5.cpp \
    util/util_path.cpp \
    util/util_simd.cpp \
    util/util_sky_model.cpp \
    util/util_string.cpp \
    util/util_system.cpp \
    util/util_task.cpp \
    util/util_thread.cpp \
    util/util_time.cpp \
    util/util_transform.cpp \
    #util/util_view.cpp \
    util/util_windows.cpp

INCLUDEPATH += ../third_party/atomic

#LIBS += $$LIB_OPENIMAGEIO

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

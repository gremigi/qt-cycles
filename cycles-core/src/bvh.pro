include(../cycles-core-common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = bvh

HEADERS += \
    $$files(bvh/*.h)

SOURCES += \
    $$files(bvh/*.cpp)

INCLUDEPATH += ../third_party/atomic
#    ../graph \
#    ../kernel \
#    ../kernel/svm \
#    ../render \
#    ../util \
#    ../device

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

include(../../common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = cuew

DEFINES += APPLICATIONLIB_LIBRARY

HEADERS += include/cuew.h

SOURCES += src/cuew.c

INCLUDEPATH += $$PWD/include

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

include(../../common.pri)

TEMPLATE = lib
CONFIG += staticlib
TARGET = clew

DEFINES += APPLICATIONLIB_LIBRARY

HEADERS += include/clew.h

SOURCES += src/clew.c

INCLUDEPATH += $$PWD/include

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

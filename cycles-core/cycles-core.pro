include(cycles-core-common.pri)

TEMPLATE = subdirs

SUBDIRS += src/util.pro
SUBDIRS += src/graph.pro
SUBDIRS += src/render.pro
SUBDIRS += src/device.pro
SUBDIRS += src/kernel.pro
SUBDIRS += src/bvh.pro
SUBDIRS += src/subd.pro

# Dependencies

util.subdir = util
device.subdir = device
kernel.subdir = kernel
graph.subdir = graph
bvh.subdir = bvh
subd.subdir = subd
render.subdir = render


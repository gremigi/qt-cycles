# Position of the root project.
TOP_SRC_DIR = $$PWD

# Position of the shadow build.
TOP_BUILD_DIR = $$shadowed($$PWD)

# Directory where to copy all built files.

QMAKE_CXXFLAGS_RELEASE += -Wall
QMAKE_CXXFLAGS_RELEASE += -Wno-sign-compare
QMAKE_CXXFLAGS_RELEASE += -fno-strict-aliasing
QMAKE_CXXFLAGS_RELEASE += -ffast-math
#QMAKE_CXXFLAGS_RELEASE += -Werror=float-conversion
#QMAKE_CXXFLAGS_RELEASE += -Werror=double-promotion
QMAKE_CXXFLAGS_RELEASE += -Wno-error=unused-macros
QMAKE_CXXFLAGS_RELEASE += -O3

DEFINES += CYCLES_STD_UNORDERED_MAP
DEFINES += "CCL_NAMESPACE_BEGIN=\"namespace ccl {\""
DEFINES += "CCL_NAMESPACE_END=\"}\""
DEFINES += WITH_CUDA_DYNLOAD
#DEFINES += WITH_CYCLES_LOGGING
#DEFINES += WITH_CYCLES_DEBUG
#DEFINES += WITH_CPU_SSE

# Directory where to copy all built files.

CONFIG(release, debug|release): {
    INSTALLDIR = $$PWD/build/release
}
CONFIG(debug, debug|release): {
    INSTALLDIR = $$PWD/build/debug
}

# Compile with GCC c++14 functionalities.
CONFIG += c++14

# This configuration prevent qmake to create debug and release directories
# inside the build directories which are already separated.
# If you remove this configuration the linker will fail built libraries.
CONFIG -= debug_and_release

# Generate libraries without links. Version is omitted if not specified.
CONFIG += plugin

INCLUDEPATH += $$PWD/third_party/atomic

win32-g++:contains(QT_ARCH, "x86_64"): {

    # Position of the custom package repository
    PACKAGE_REPO = C:\msys64\mingw64\local\dst

    #OpenImageIO (compiled to reduce the huge number of dependendencies)
    INCLUDEPATH += $$quote($$PACKAGE_REPO/openimageio/include)
}

macx: {

    # Position of the custom package repository
    PACKAGE_REPO = /Users/robot/repo/mac

    INCLUDEPATH += /usr/local/include

    #OpenImageIO (compiled to reduce the huge number of dependendencies)
    INCLUDEPATH += $$quote($$PACKAGE_REPO/openimageio/include)
}

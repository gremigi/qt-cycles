# Position of the root project.
TOP_SRC_DIR = $$PWD

# Position of the shadow build.
TOP_BUILD_DIR = $$shadowed($$PWD)

# Directory where to copy all built files.

CONFIG(release, debug|release): {
    INSTALLDIR = $$PWD/build/release
}
CONFIG(debug, debug|release): {
    INSTALLDIR = $$PWD/build/debug
}

QMAKE_CXXFLAGS_RELEASE += -g # Add debug info into the release.
QMAKE_CFLAGS_RELEASE += -g # Add debug info into the release.
QMAKE_LFLAGS_RELEASE +=

greaterThan(QT_MAJOR_VERSION, 5)

# Compile with GCC c++14 functionalities.
CONFIG += c++14

# This configuration prevent qmake to create debug and release directories
# inside the build directories which are already separated.
# If you remove this configuration the linker will fail built libraries.
CONFIG -= debug_and_release

# Generate libraries without links. Version is omitted if not specified.
CONFIG += plugin

# When building on Mac don't store the executable into a bundle.
# It also requires a different stripping method.
macx: {
    CONFIG -= app_bundle
}

INCLUDEPATH += \
    $$TOP_SRC_DIR/cycles

win32-g++:contains(QT_ARCH, "x86_64"): {

    # Position of the custom package repository
    PACKAGE_REPO = C:\msys64\mingw64\local\dst

    #OpenImageIO (compiled to reduce the huge number of dependendencies)
    INCLUDEPATH += $$quote($$PACKAGE_REPO/openimageio/include)
    LIBS += -L$$quote($$PACKAGE_REPO/openimageio/bin)
    LIB_OPENIMAGEIO = -lOpenImageIO

    # Cycles
    LIB_GLEW = -lglew32
    LIB_GLUT = -lfreeglut
}

macx: {

    # Position of the custom package repository
    PACKAGE_REPO = /Users/robot/repo/mac

    #OpenImageIO
    LIB_OPENIMAGEIO = -lOpenImageIO

    # Cycles
    LIB_GLEW = -lGLEW
    LIB_GLUT = -lglut
}

unix:!macx {

    # Position of the custom package repository
    PACKAGE_REPO = /home/giovanni/repo/unix64

    # Tell the linker to load a dynamic library from the executable location
    # instead of using LD_LIBRARY_PATH.
    # This only work for the executables/libraries built here and not for
    # inderect dependencies like the one requested by shipped Qt libraries.

    # Use RUNPATH instead of RPATH
    #CONFIG += enable_new_dtags

    # Suppress the default RPATH (or RUNPATH) if you wish
    QMAKE_LFLAGS_RPATH=

    # Add your own RPATH (or RUNPATH) with quoting gyrations to make sure $ORIGIN gets to the command line unexpanded.
    QMAKE_LFLAGS += "-Wl,-rpath,\'\$$ORIGIN\'"
    QMAKE_LFLAGS += "-Wl,-rpath,\'\$$ORIGIN/lib\'"

    #OpenImageIO
    LIB_OPENIMAGEIO = -lOpenImageIO

    LIBS += -l:libicui18n.so.56
    LIBS += -l:libicuuc.so.56
    LIBS += -l:libicudata.so.56
    LIBS += -lQt5XcbQpa
    LIBS += -lQt5DBus
    LIBS += -lQt5Core
    LIBS += -lQt5Gui
    LIBS += -lQt5Widgets
    LIBS += -lQt5Test
    LIBS += -lQt5Svg
    LIBS += -lQt5Xml

    # Cycles
    LIB_GLEW = -lGLEW
    LIB_GLUT = -lglut
}

LIBS += -L$$quote($$TOP_BUILD_DIR/cycles)
LIB_CYCLES = -lCycles

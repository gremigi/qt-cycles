#include "SceneFactory.h"

#include "util/util_transform.h"
#include "device/device.h"
#include "render/object.h"
#include "render/shader.h"
#include "render/graph.h"
#include "render/nodes.h"
#include "render/film.h"

SceneFactory::SceneFactory()
{
}

ccl::Scene *SceneFactory::createScene(const ccl::DeviceInfo &deviceInfo,
        const QString &imageFile) {

    ccl::SceneParams sceneParams;
    ccl::Scene *scene = new ccl::Scene{sceneParams, deviceInfo};
    scene->params.bvh_type = ccl::SceneParams::BVH_DYNAMIC;

    ccl::Camera *camera = scene->camera;
    camera->width = CAMERA_WIDTH;
    camera->height = CAMERA_HEIGHT;
    camera->fov = ccl::radians(CAMERA_FOV_DEG);
    camera->type = ccl::CameraType::CAMERA_PERSPECTIVE;
    camera->full_width = camera->width;
    camera->full_height = camera->height;
    scene->film->exposure = 1.0f;

    // Find the distance to perfectly frame the lens with a 5% border.
    float size = 1.05f * CUBE_SIZE;
    float distance = 2.0f * size / (2.0f * std::tan(camera->fov / 2.0f));

    camera->matrix = ccl::transform_identity()
        * ccl::transform_translate(ccl::make_float3(0.0f, 0.0f, -distance));
    camera->need_update = true;
    camera->update();

    // Object shader

    ccl::Shader *objectShader = createCubeShader(imageFile);
    objectShader->tag_update(scene);
    scene->shaders.push_back(objectShader);

    // Cube

    ccl::Mesh *objectMesh = createCube(CUBE_SIZE);
    objectMesh->used_shaders.push_back(objectShader);
    scene->meshes.push_back(objectMesh);

    ccl::Object *object = new ccl::Object();
    object->name = "cubeObject";
    object->mesh = objectMesh;
    object->tfm = ccl::transform_identity()
        * ccl::transform_rotate(40.0f * 3.14f / 180.0f, ccl::make_float3(1.0f, 1.0f, 1.0f));
    scene->objects.push_back(object);

    return scene;
}

ccl::Shader *SceneFactory::createCubeShader(const QString &imageFile) {

    ccl::Shader *shader = new ccl::Shader();
    shader->name = "cubeShader";
    ccl::ShaderGraph *shaderGraph = new ccl::ShaderGraph();

    const ccl::NodeType *imageTextureNodeType = ccl::NodeType::find(ccl::ustring("image_texture"));
    ccl::ShaderNode *imageTextureShaderNode = static_cast<ccl::ShaderNode*>(imageTextureNodeType->create(imageTextureNodeType));
    imageTextureShaderNode->name = "imageTextureNode";
    ccl::ImageTextureNode *img = static_cast<ccl::ImageTextureNode*>(imageTextureShaderNode);
    img->filename = ccl::ustring(imageFile.toStdString());

    shaderGraph->add(imageTextureShaderNode);

    const ccl::NodeType *emissionNodeType = ccl::NodeType::find(ccl::ustring("emission"));
    ccl::ShaderNode *emissionShaderNode = static_cast<ccl::ShaderNode*>(emissionNodeType->create(emissionNodeType));
    emissionShaderNode->name = "emissionNode";
    emissionShaderNode->set(
        *emissionShaderNode->type->find_input(ccl::ustring("strength")),
        1.0f
    );
    shaderGraph->add(emissionShaderNode);

    shaderGraph->connect(
        imageTextureShaderNode->output("Color"),
        emissionShaderNode->input("Color")
    );
    shaderGraph->connect(
        emissionShaderNode->output("Emission"),
        shaderGraph->output()->input("Surface")
    );

    shader->set_graph(shaderGraph);
    return shader;
}

ccl::Mesh *SceneFactory::createCube(float size) {

    ccl::Mesh *mesh = new ccl::Mesh();
    mesh->subdivision_type = ccl::Mesh::SUBDIVISION_NONE;

    // 8 vertices.

    ccl::vector<ccl::float3> P;
    P.push_back(ccl::make_float3( size * 0.5f,  size * 0.5f, -size * 0.5f));
    P.push_back(ccl::make_float3( size * 0.5f, -size * 0.5f, -size * 0.5f));
    P.push_back(ccl::make_float3(-size * 0.5f, -size * 0.5f, -size * 0.5f));
    P.push_back(ccl::make_float3(-size * 0.5f,  size * 0.5f, -size * 0.5f));
    P.push_back(ccl::make_float3( size * 0.5f,  size * 0.5f,  size * 0.5f));
    P.push_back(ccl::make_float3( size * 0.5f, -size * 0.5f,  size * 0.5f));
    P.push_back(ccl::make_float3(-size * 0.5f, -size * 0.5f,  size * 0.5f));
    P.push_back(ccl::make_float3(-size * 0.5f,  size * 0.5f,  size * 0.5f));

    // 6 surfaces of 4 vertices each.

    ccl::vector<size_t> surfaces;
    surfaces.push_back(4);
    surfaces.push_back(4);
    surfaces.push_back(4);
    surfaces.push_back(4);
    surfaces.push_back(4);
    surfaces.push_back(4);

    // Define the 6 surfaces.

    ccl::vector<int> verts;

    verts.push_back(0);
    verts.push_back(1);
    verts.push_back(2);
    verts.push_back(3);

    verts.push_back(4);
    verts.push_back(7);
    verts.push_back(6);
    verts.push_back(5);

    verts.push_back(0);
    verts.push_back(4);
    verts.push_back(5);
    verts.push_back(1);

    verts.push_back(1);
    verts.push_back(5);
    verts.push_back(6);
    verts.push_back(2);

    verts.push_back(2);
    verts.push_back(6);
    verts.push_back(7);
    verts.push_back(3);

    verts.push_back(4);
    verts.push_back(0);
    verts.push_back(3);
    verts.push_back(7);

    // 8 vertices and 12 triangles (2 per cube surface).

    mesh->verts = P;
    mesh->reserve_mesh(8, 12);

    /* create triangles */
    size_t offset = 0;
    for(size_t i = 0; i < surfaces.size(); i++) {
        for(size_t j = 0; j < surfaces[i] - 2; j++) {
            int v0 = verts[offset];
            int v1 = verts[offset + j + 1];
            int v2 = verts[offset + j + 2];
            mesh->add_triangle(v0, v1, v2, 0, false);
        }
        offset += surfaces[i];
    }

    ccl::Attribute *attr = mesh->attributes.add(ccl::ATTR_STD_UV, ccl::ustring("UVMap"));
    ccl::float3 *fdata = attr->data_float3();

    /* loop over the triangles */
    offset = 0;
    for(size_t i = 0; i < surfaces.size(); i++) {

        fdata[0] = ccl::make_float3(1.0, 1.0, 0.0);
        fdata[1] = ccl::make_float3(1.0, 0.0, 0.0);
        fdata[2] = ccl::make_float3(0.0, 0.0, 0.0);

        fdata[3] = ccl::make_float3(1.0, 1.0, 0.0);
        fdata[4] = ccl::make_float3(0.0, 0.0, 0.0);
        fdata[5] = ccl::make_float3(0.0, 1.0, 0.0);

        fdata += 6;
        offset += surfaces[i];
    }

    return mesh;
}

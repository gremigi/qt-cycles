#ifndef CYCLESWIDGETIMPL_H
#define CYCLESWIDGETIMPL_H

#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QWheelEvent>

#include <memory>

#include "render/session.h"

class CyclesWidgetImpl : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    CyclesWidgetImpl(QWidget *parent = nullptr);
    virtual ~CyclesWidgetImpl();

    void setScene(const QString &imageFile);

protected:

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void paintEvent(QPaintEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

signals:
    void updateRequired();

private:

    void displayInfo(QPainter &painter, ccl::Progress &progress);
    void resetScene(int width, int height);

    ccl::DeviceDrawParams drawParams;
    ccl::SessionParams sessionParams;
    ccl::BufferParams bufferParams;

    std::unique_ptr<ccl::Session> session;

    QPoint mouseClickPoint;
    QPointF cubeWorldPosition;
};

#endif // CYCLESWIDGETIMPL_H

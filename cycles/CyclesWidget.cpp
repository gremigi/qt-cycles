#include "CyclesWidget.h"

#include "CyclesWidgetImpl.h"

#include <QHBoxLayout>

CyclesWidget::CyclesWidget(QWidget *parent) : QWidget(parent)
{
    pimpl = new CyclesWidgetImpl{this};
    QHBoxLayout *layout = new QHBoxLayout{this};
    layout->setMargin(0);
    layout->addWidget(pimpl);
    setLayout(layout);
}

void CyclesWidget::setScene(const QString &imageFile)
{
    pimpl->setScene(imageFile);
}

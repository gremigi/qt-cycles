include(../common.pri)

QT += core widgets gui

TEMPLATE = lib
TARGET = Cycles
#CONFIG += staticlib

QMAKE_CXXFLAGS_RELEASE += -Wall
QMAKE_CXXFLAGS_RELEASE += -Wno-sign-compare
QMAKE_CXXFLAGS_RELEASE += -fno-strict-aliasing
QMAKE_CXXFLAGS_RELEASE += -ffast-math
QMAKE_CXXFLAGS_RELEASE += -Wno-error=unused-macros
QMAKE_CXXFLAGS_RELEASE += -O3

DEFINES += APPLICATIONLIB_LIBRARY
DEFINES += CYCLES_STD_UNORDERED_MAP
DEFINES += "CCL_NAMESPACE_BEGIN=\"namespace ccl {\""
DEFINES += "CCL_NAMESPACE_END=\"}\""
DEFINES += WITH_CUDA_DYNLOAD
#DEFINES += WITH_CYCLES_LOGGING
#DEFINES += WITH_CYCLES_DEBUG
#DEFINES += WITH_CPU_SSE

HEADERS += \
    SceneFactory.h \
    CyclesWidget.h \
    CyclesWidgetImpl.h

SOURCES += \
    SceneFactory.cpp \
    CyclesWidget.cpp \
    CyclesWidgetImpl.cpp

INCLUDEPATH += ../cycles-core/third_party/atomic \
    ../cycles-core/src

# IMPORTANT: the order of libs is important.
LIBS += \
    -L$$TOP_BUILD_DIR/cycles-core/src -lrender \
    -L$$TOP_BUILD_DIR/cycles-core/src -lutil \
    -L$$TOP_BUILD_DIR/cycles-core/src -lgraph \
    -L$$TOP_BUILD_DIR/cycles-core/src -lbvh \
    -L$$TOP_BUILD_DIR/cycles-core/src -ldevice \
    -L$$TOP_BUILD_DIR/cycles-core/src -lsubd \
    -L$$TOP_BUILD_DIR/cycles-core/src -lkernel

LIBS += $$LIB_OPENIMAGEIO $$LIB_GLUT $$LIB_GLEW
win32-g++:contains(QT_ARCH, "x86_64"): {
    LIBS += -lopengl32
}

# target is a special install target that does not require to specify files.
target.path = $$INSTALLDIR
INSTALLS += target

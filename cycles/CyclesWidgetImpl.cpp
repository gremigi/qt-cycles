#include "CyclesWidgetImpl.h"

#include <QtDebug>

#include <QPainter>
#include <QRect>

#include "device/device.h"
#include "render/camera.h"
#include "render/object.h"
#include "SceneFactory.h"

CyclesWidgetImpl::CyclesWidgetImpl(QWidget *parent) : QOpenGLWidget(parent)
{
    QSurfaceFormat  format;
    format.setDepthBufferSize(24);
    format.setProfile(QSurfaceFormat::OpenGLContextProfile::CompatibilityProfile);
    format.setSwapInterval(0); // Remove flickering when resizing.
    QSurfaceFormat::setDefaultFormat(format);
    setFormat(format);
    setAutoFillBackground(false);

    connect(this, SIGNAL(updateRequired()), this, SLOT(update()), Qt::ConnectionType::QueuedConnection);
}

CyclesWidgetImpl::~CyclesWidgetImpl()
{

}

void CyclesWidgetImpl::setScene(const QString &imageFile)
{
    mouseClickPoint.setX(0);
    mouseClickPoint.setY(0);

    cubeWorldPosition.setX(0.0);
    cubeWorldPosition.setY(0.0);

    ccl::DeviceType deviceType = ccl::Device::type_from_string("cpu");
    ccl::vector<ccl::DeviceInfo>& devices = ccl::Device::available_devices();

    ccl::DeviceInfo deviceInfo;
    for (const ccl::DeviceInfo& device : devices) {
        if(deviceType == device.type) {
            deviceInfo = device;
            break;
        }
    }

    sessionParams.progressive = true;
    sessionParams.start_resolution = 64;
    sessionParams.device = deviceInfo;
    sessionParams.samples = 20;

    ccl::Scene *scene = SceneFactory::createScene(deviceInfo, imageFile);

    session = std::make_unique<ccl::Session>(sessionParams);
    session->scene = scene;

    session->progress.set_update_callback([this]() { // Attention: this is called my a thread.
        emit updateRequired();
    });

    resetScene(width(), height()) ;
    session->start();
}

void CyclesWidgetImpl::resetScene(int width, int height) {

    bufferParams.width = width;
    bufferParams.height = height;
    bufferParams.full_width = width;
    bufferParams.full_height = height;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, bufferParams.width, 0, bufferParams.height, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (session) {
        session->scene->camera->width = width;
        session->scene->camera->height = height;
        session->scene->camera->compute_auto_viewplane();
        session->scene->camera->need_update = true;
        session->scene->camera->need_device_update = true;
        session->reset(bufferParams, sessionParams.samples);
    }
}

void CyclesWidgetImpl::paintEvent(QPaintEvent *event)
{
    event->accept();
    paintGL();
}

void CyclesWidgetImpl::initializeGL() {
    initializeOpenGLFunctions();
    glClearColor(0.05f, 0.05f, 0.05f, 0.0f);
}

void CyclesWidgetImpl::resizeGL(int width, int height) {

    if (width <= 0 || height <= 0) {
        return;
    }
    resetScene(width, height);
    update();
}

void CyclesWidgetImpl::paintGL() {

    QPainter painter;

    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (session) {
        session->draw(bufferParams, drawParams);
        displayInfo(painter, session->progress);
    }

    painter.end();
}

void CyclesWidgetImpl::displayInfo(QPainter &painter, ccl::Progress& progress)
{
    int tile;
    double totalTime;
    double  sampleTime;
    double  renderTime;
    std::string status;
    std::string substatus;

    //progress.get_tile(tile, totalTime, sampleTime, renderTime);
    progress.get_status(status, substatus);

    if (substatus != "") {
        status += ": " + substatus;
    }

    std::string str = ccl::string_printf("%s        Time: %.2f", status.c_str(), totalTime);

    QPen penHText(QColor("white"));
    painter.setPen(penHText);

    const QRect rectangle = QRect(0.0f, 0.0f, bufferParams.width, 20);
    painter.drawText(rectangle, Qt::AlignHCenter | Qt::AlignVCenter, QString::fromStdString(str));
}

void CyclesWidgetImpl::mousePressEvent(QMouseEvent *event)
{
    if (!session) {
        return;
    }

    event->accept();
    mouseClickPoint.setX(this->mapFromGlobal(QCursor::pos()).x());
    mouseClickPoint.setY(this->mapFromGlobal(QCursor::pos()).y());
}

void CyclesWidgetImpl::mouseMoveEvent(QMouseEvent *event)
{
    if (!session) {
        return;
    }

    event->accept();

    if (event->buttons() & Qt::LeftButton) {

        float dxPixels = static_cast< float>(mouseClickPoint.x() - this->mapFromGlobal(QCursor::pos()).x());
        float dyPixels = static_cast<float>(mouseClickPoint.y() - this->mapFromGlobal(QCursor::pos()).y());

        float distance = std::abs(session->scene->camera->matrix.z[3]);
        float size = 2.0f * std::tan(0.5f * session->scene->camera->fov) * distance;

        float dxInches = size * dxPixels / static_cast<float>(height());
        float dyInches = size * dyPixels / static_cast<float>(height());

        ccl::Transform matrix = ccl::transform_identity()
            * ccl::transform_translate(ccl::make_float3(
                static_cast<float>(cubeWorldPosition.x()) + dxInches,
                static_cast<float>(cubeWorldPosition.y()) - dyInches,
                -distance));

        /* Update and Reset */
        session->scene->camera->matrix = matrix;
        session->scene->camera->need_update = true;
        session->scene->camera->need_device_update = true;

        session->reset(bufferParams, sessionParams.samples);
    }
    update();
}

void CyclesWidgetImpl::mouseReleaseEvent(QMouseEvent *event)
{
    if (!session) {
        return;
    }

    event->accept();
    ccl::Transform matrix = session->scene->camera->matrix;
    cubeWorldPosition.setX(static_cast<double>(matrix.x[3]));
    cubeWorldPosition.setY(static_cast<double>(matrix.y[3]));
}

void CyclesWidgetImpl::wheelEvent(QWheelEvent *event)
{
    if (!session) {
        return;
    }

    event->accept();
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;

    QPoint delta;
    if (!numPixels.isNull()) {
        delta = numPixels;
    } else if (!numDegrees.isNull()) {
        QPoint numSteps = numDegrees / 15;
        delta = numSteps;
    }

    session->scene->camera->fov -= delta.y() / 20.0f;
    session->scene->camera->need_update = true;
    session->scene->camera->need_device_update = true;

    session->reset(bufferParams, sessionParams.samples);
    update();
}

#ifndef CYCLESWIDGET_H
#define CYCLESWIDGET_H

#include "QWidget"

class CyclesWidgetImpl;

class CyclesWidget : public QWidget
{
    Q_OBJECT

public:
    CyclesWidget(QWidget *parent = nullptr);
    virtual ~CyclesWidget() = default;

    void setScene(const QString &imageFile);

private:
    CyclesWidgetImpl *pimpl;

};

#endif // CYCLESWIDGET_H

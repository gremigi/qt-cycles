#ifndef SCENEFACTORY_H
#define SCENEFACTORY_H

#include "render/scene.h"
#include "render/mesh.h"
#include "render/camera.h"

#include <QString>

class SceneFactory
{
public:
    SceneFactory();
    static ccl::Scene *createScene(const ccl::DeviceInfo &deviceInfo,
        const QString &imageFile);

private:
    static ccl::Mesh *createCube(float size);
    static ccl::Shader *createCubeShader(const QString &imageFile);

    static const int CAMERA_WIDTH = 100;
    static const int CAMERA_HEIGHT = 100;
    static constexpr float CUBE_SIZE = 1.0f;
    static constexpr float CAMERA_FOV_DEG = 45.0f;
};

#endif // SCENEFACTORY_H

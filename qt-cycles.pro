include(common.pri)

TEMPLATE = subdirs

SUBDIRS += cycles
SUBDIRS += cycles-core
SUBDIRS += cycles-app

# Dependencies

cycles.subdir = cycles
cycles.depends = cycles-core

cycles-app.subdir = cycles-app
cycles-app.depends = cycles
